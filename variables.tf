variable "project_name" {
  description = "(String) The name of the project."
  type        = string
}

variable "project_description" {
  description = "(String) A description of the project."
  type        = string
  default     = null
}

variable "project_build_timeout" {
  description = "(Number) The maximum amount of time, in seconds, that a job can run."
  type        = number
  default     = 6500
}

variable "project_builds_access_level" {
  description = "(String) Set the builds access level. Valid values are disabled, private, enabled."
  type        = string
  default     = "private"
}

variable "project_default_branch" {
  description = "(String) The default branch for the project."
  type        = string
  default     = "main"
}

variable "project_initialize_with_readme" {
  description = "(Boolean) Create main branch with first commit containing a README.md file."
  type        = bool
  default     = true
}

variable "project_issues_access_level" {
  description = "(String) Set the issues access level. Valid values are disabled, private, enabled."
  type        = string
  default     = "private"
}

variable "project_issues_enabled" {
  description = " (Boolean) Enable issue tracking for the project."
  type        = bool
  default     = true
}

variable "project_merge_requests_enabled" {
  description = "(Boolean) Enable merge requests for the project."
  type        = bool
  default     = true
}

variable "project_path" {
  description = " (String) The path of the repository."
  type        = string
  default     = null
}

variable "project_public_builds" {
  description = "(Boolean) If true, jobs can be viewed by non-project members."
  type        = bool
  default     = false
}

variable "project_repository_access_level" {
  description = " (String) Set the repository access level. Valid values are disabled, private, enabled."
  type        = string
  default     = "private"
}

variable "project_request_access_enabled" {
  description = "(Boolean) Allow users to request member access."
  type        = bool
  default     = false
}

variable "project_shared_runners_enabled" {
  description = " (Boolean) Enable shared runners for this project."
  type        = bool
  default     = false
}

variable "project_snippets_access_level" {
  description = "(String) Set the snippets access level. Valid values are disabled, private, enabled."
  type        = string
  default     = "private"
}

variable "project_snippets_enabled" {
  description = "(Boolean) Enable snippets for the project."
  type        = bool
  default     = false
}

variable "project_topics" {
  description = "(Set of String) The list of topics for the project."
  type        = list(string)
  default     = null
}

variable "project_visibility_level" {
  description = "(String) Set to public to create a public project."
  type        = string
  default     = null
}

variable "project_wiki_access_level" {
  description = "(String) Set the wiki access level. Valid values are disabled, private, enabled."
  type        = string
  default     = "private"
}

variable "project_wiki_enabled" {
  description = "(Boolean) Enable wiki for the project."
  type        = bool
  default     = false
}
