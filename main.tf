resource "gitlab_project" "this" {
  name        = var.project_name
  description = var.project_description

  build_timeout           = var.project_build_timeout
  builds_access_level     = var.project_builds_access_level
  default_branch          = var.project_default_branch
  initialize_with_readme  = var.project_initialize_with_readme
  issues_access_level     = var.project_issues_access_level
  issues_enabled          = var.project_issues_enabled
  merge_requests_enabled  = var.project_merge_requests_enabled
  path                    = var.project_path
  public_builds           = var.project_public_builds
  repository_access_level = var.project_repository_access_level
  request_access_enabled  = var.project_request_access_enabled
  shared_runners_enabled  = var.project_shared_runners_enabled
  snippets_access_level   = var.project_snippets_access_level
  snippets_enabled        = var.project_snippets_enabled
  topics                  = var.project_topics
  visibility_level        = var.project_visibility_level
  wiki_access_level       = var.project_wiki_access_level
  wiki_enabled            = var.project_wiki_enabled
}
