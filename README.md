# Gitlab Module



<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | 3.13.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | 3.13.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_project.this](https://registry.terraform.io/providers/gitlabhq/gitlab/3.13.0/docs/resources/project) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_project_build_timeout"></a> [project\_build\_timeout](#input\_project\_build\_timeout) | (Number) The maximum amount of time, in seconds, that a job can run. | `number` | `6500` | no |
| <a name="input_project_builds_access_level"></a> [project\_builds\_access\_level](#input\_project\_builds\_access\_level) | (String) Set the builds access level. Valid values are disabled, private, enabled. | `string` | `"private"` | no |
| <a name="input_project_default_branch"></a> [project\_default\_branch](#input\_project\_default\_branch) | (String) The default branch for the project. | `string` | `"main"` | no |
| <a name="input_project_description"></a> [project\_description](#input\_project\_description) | (String) A description of the project. | `string` | `null` | no |
| <a name="input_project_initialize_with_readme"></a> [project\_initialize\_with\_readme](#input\_project\_initialize\_with\_readme) | (Boolean) Create main branch with first commit containing a README.md file. | `bool` | `true` | no |
| <a name="input_project_issues_access_level"></a> [project\_issues\_access\_level](#input\_project\_issues\_access\_level) | (String) Set the issues access level. Valid values are disabled, private, enabled. | `string` | `"private"` | no |
| <a name="input_project_issues_enabled"></a> [project\_issues\_enabled](#input\_project\_issues\_enabled) | (Boolean) Enable issue tracking for the project. | `bool` | `true` | no |
| <a name="input_project_merge_requests_enabled"></a> [project\_merge\_requests\_enabled](#input\_project\_merge\_requests\_enabled) | (Boolean) Enable merge requests for the project. | `bool` | `true` | no |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | (String) The name of the project. | `string` | n/a | yes |
| <a name="input_project_path"></a> [project\_path](#input\_project\_path) | (String) The path of the repository. | `string` | `null` | no |
| <a name="input_project_public_builds"></a> [project\_public\_builds](#input\_project\_public\_builds) | (Boolean) If true, jobs can be viewed by non-project members. | `bool` | `false` | no |
| <a name="input_project_repository_access_level"></a> [project\_repository\_access\_level](#input\_project\_repository\_access\_level) | (String) Set the repository access level. Valid values are disabled, private, enabled. | `string` | `"private"` | no |
| <a name="input_project_request_access_enabled"></a> [project\_request\_access\_enabled](#input\_project\_request\_access\_enabled) | (Boolean) Allow users to request member access. | `bool` | `false` | no |
| <a name="input_project_shared_runners_enabled"></a> [project\_shared\_runners\_enabled](#input\_project\_shared\_runners\_enabled) | (Boolean) Enable shared runners for this project. | `bool` | `false` | no |
| <a name="input_project_snippets_access_level"></a> [project\_snippets\_access\_level](#input\_project\_snippets\_access\_level) | (String) Set the snippets access level. Valid values are disabled, private, enabled. | `string` | `"private"` | no |
| <a name="input_project_snippets_enabled"></a> [project\_snippets\_enabled](#input\_project\_snippets\_enabled) | (Boolean) Enable snippets for the project. | `bool` | `false` | no |
| <a name="input_project_topics"></a> [project\_topics](#input\_project\_topics) | (Set of String) The list of topics for the project. | `list(string)` | `null` | no |
| <a name="input_project_visibility_level"></a> [project\_visibility\_level](#input\_project\_visibility\_level) | (String) Set to public to create a public project. | `string` | `null` | no |
| <a name="input_project_wiki_access_level"></a> [project\_wiki\_access\_level](#input\_project\_wiki\_access\_level) | (String) Set the wiki access level. Valid values are disabled, private, enabled. | `string` | `"private"` | no |
| <a name="input_project_wiki_enabled"></a> [project\_wiki\_enabled](#input\_project\_wiki\_enabled) | (Boolean) Enable wiki for the project. | `bool` | `false` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
